<!-- BEGIN xnat-templates/screens/Login.vm -->
#if ($data.getMessage())
  <div class="warning">$data.getMessage() <img src="$content.getURI('/images/close.gif')" style="position: absolute; right: 10px;" onclick="javascript:$(this).parent('div').slideUp(200)" /></div>
#end
#if($msg)
  <div class="warning">$msg <img src="$content.getURI('/images/close.gif')" style="position: absolute; right: 10px;" onclick="javascript:$(this).parent('div').slideUp(200)" /></div>
#end	

<div id="page-bg"></div> <!-- stores and masks background image for login page -->

<div class="data-banner">
  <div class="header">
	<h1 id="page-title">Access HCP Data Releases</h1>
	<!-- site description vm -->
	#parse("/screens/site_description.vm")
	<!-- end site description -->
  </div>

  <!--  <div id="user-info"></div> -->
  <div class="overlay-container">
      <div class="overlay-half">
      	<h3>Log In</h3>
        <form class="friendlyForm" method="post" action="$content.getURI('/j_spring_security_check')" accept-charset="UTF-8">
        <input type="hidden" id="login_method" name="login_method" value="$login_methods.get(0)" />
        	<p><label for="username">Username</label> <input type="text" id="username" name="j_username" /></p>
            <p><label for="j_password">Password</label> <input type="password" id="j_password" name="j_password" autocomplete="off" /></p>
            <div class="overlay-action"><a id="forgotMyLogin" class="forgotMyLogin" style="margin-right:30px;">Forgot username or password?</a> <input type="submit" value="Log In" /></div>
		</form>
     </div>
     
      <div class="overlay-half right">
      	<h3>Create an Account</h3>
        <p>All public releases of data and software tools produced by the Human Connectome Project require a simple sign-on to access. Registering for an account is free and your information will not be shared with any third parties, as per our <a href="http://humanconnectome.org/privacy/" target="_new">privacy policy</a>.</p>
        <div class="overlay-action"><input type="button" class="registerMe" id="registerMe" value="Register" /></div>
     </div>
  </div>
  
</div>
<!-- end banner -->

<!-- begin masked content for overlay -->

<!-- modal initialization -->

<link type="text/css" rel="stylesheet" href="$content.getURI('/scripts/xModal/xModal.css')">
<script type="text/javascript" src="$content.getURI('/scripts/xModal/xModal.js')"></script>
<script type="text/javascript" src="$content.getURI('/scripts/data-access.js')"></script>

<script type="text/javascript">
$(document).ready(function(){
	var page_body = "#page_body" ;

// selector for what you're clicking to open another modal
		// registration 
        $(page_body).on('click','#registerMe',function(){
			
			<!-- (re)initialize registration form -->
			$('#reg-div').removeClass('hidden'); 
			$('#regSubmit').addClass('hidden');
			$('.required_text').removeClass('hidden');
			
            xModalOpen({
                kind: 'fixed',  // size - 'fixed','custom','large','med','small',
                width: 700,  // box width when used with 'fixed' size, horizontal margin when used with 'custom' size
                height: 550,  // box height when used with 'fixed' size, vertical margin when used with 'custom' size
                box: 'register_modal',  // id of static content container
				scroll: 'yes',
                title: 'Register an account with the Human Connectome Project', // text for modal title bar
                content: 'static',
                footer: {
                    show: 'yes',// 'static' xModal has footer in HTML
                    content: '', // empty string renders default footer with two buttons (ok/cancel) using values specified below
                    background: '#e0e0e0',
                    border: '#e0e0e0'
                },
				ok: 'Submit',
				cancel: 'Cancel'
            });

            xModalSubmit = function(){
                // what to do when the thing is submitted
                var form = document.getElementById('regForm');
                form.submit();
                // if you want the page to close, do so here:
                xModalClose();
            };

            xModalCancel = function(){
                xModalClose();
            };

        });	
		
		// login / password reminder
		$(page_body).on('click','#forgotMyLogin',function(){
		
			xModalOpen({
                kind: 'fixed',  // size - 'fixed','custom','large','med','small',
                width: 700,  // box width when used with 'fixed' size, horizontal margin when used with 'custom' size
                height: 320,  // box height when used with 'fixed' size, vertical margin when used with 'custom' size
                box: 'forgot_modal',  // id of static content container
                title: 'ConnectomeDB Username / Password Reminder', // text for modal title bar
                content: 'static',
                //content: $('#forgot-pass-div').html(), // main body of modal window
                footer: {
                    show: 'yes',// 'static' xModal has footer in HTML
                    content: '', // empty string renders default footer with two buttons (ok/cancel) using values specified below
					background: '#f0f0f0',
					border: '#e0e0e0'
                },
				ok: 'Request',
				cancel: 'Cancel'
            });

            xModalSubmit = function(){
                // what to do when the thing is submitted
                $('#forgotForm').submit(); 
                // if you want the page to close, do so here:
                xModalClose();
            };

            xModalCancel = function(){
                xModalClose();
            };
		});
		$("#username").focus();
		$('#register_modal .footer').css('background-color','#e0e0e0');
	});

</script>



<!-- MODAL: Registration form. -->


<div id="register_modal" class="x_modal static">
    <div class="box round scroll">
        <div class="title round">
            <span class="inner">Register an account with the Human Connectome Project.</span>  <!-- title -->
            <div class="close cancel button"></div>
        </div>
        <div class="body">
            <div class="inner">
                <!-- modal body start -->
                <div id="reg-div">
                    <form class="friendlyForm" id="regForm" action='javascript:dbRegisterCall("$content.getURI("/registerUser")");' accept-charset="UTF-8">
                        <p><strong>Registration</strong></p>
                        <div class="inputWrapper">
                            <label for="username">Create a username</label> <input type="text" name="username" maxlength="20" class="required" />
                            <div class="helptext">Usernames must be all lower-case.</div>
                        </div>
                        <p class="error hidden" id="duplicate-user">That username already exists</p>
                        <div class="inputWrapper">
                            <label for="pw">Create a password</label> <input type="password" id="pw" name="pw" maxlength="127" class="required" />
                            <div class="helptext">Passwords must be 5 or more characters.</div>
                        </div>
                        <p class="error hidden" id="password-simple">Password must be 5 or more characters.</p>
                        <div class="inputWrapper">
                            <label for="pwc">Confirm your password</label> <input type="password" id="pwc" name="pwc" maxlength="127" class="required" />
                            <div class="helptext">Passwords must match.</div>
                        </div>
                        <p class="error hidden" id="password-mismatch">Passwords do not match!</p>
                        <hr size="1" color="#ccc" />
                        <p><strong>Account Information</strong></p>
                        <div class="inputWrapper">
                            <label for="firstname">First Name</label> <input type="text" id="firstname" name="firstname" maxlength="63" class="required" />
                        </div>
                        <div class="inputWrapper">
                            <label for="lastname">Last Name</label> <input type="text" id="lastname" name="lastname" maxlength="63" class="required" />
                        </div>
                        <div class="inputWrapper">
                            <label for="email">Email</label> <input type="text" id="email" name="email" maxlength="127" class="email required" />
                            <div class="helptext">Your email is required to complete registration for this site.</div>
                        </div>
                        <p id="email-error" class="error hidden">Improper email entered.</p>
                        <p id="duplicate-email" class="error hidden">That email address is already registered.</p>
                        <div class="inputWrapper">
                            <label for="institution">Institution</label> <input type="text" id="institution" name="institution" maxlength="63" class="required" />
                            <div class="helptext"></div>
                        </div>
                        <!--
                        <div class="inputWrapper department">
                            <label for="department">Department (Leave this blank)</label> <input type="text" name="department" />
                        </div> -->
                        <input type="hidden" name="department" id="reg-department"/>
                        <input type="hidden" name="isWBForm" value="N"/>
                        <input type="hidden" name="agreeToDataUseTerms" id="agreeToDataUseTerms"/>
                        <input type="hidden" name="agreeToWBTerms" id="agreeToWBTerms"/>
                        <hr size="1" color="#ccc" />
                        <p><strong>Mailing List Subscriptions (Optional)</strong></p>
                        <p><input type="checkbox" id="hcp-announce" name="hcp-announce" checked /> Send me announcements of future data releases<br />
                            <input type="checkbox" id="hcp-users" name="hcp-users" checked /> Subscribe me to the HCP Data Users email forum</p>
                    </form>
                </div>


                <!-- modal body end -->


            </div>
        </div>
        <div class="footer">
            <div class="inner">
				<span class="required_text message">All fields required for registration.</span>
                <span class="buttons">
                    <a class="cancel button" href="javascript:">Cancel</a>  <!-- "Cancel" button text -->
                    <a class="ok default button hidden" id="regSubmit" href="javascript:">Submit</a>  <!-- "OK" button text -->
                </span>
            </div>
        </div>
    </div>
</div>

<form id="dataUsersListForm" action="http://lists.humanconnectome.org/mailman/subscribe/hcp-users" target="oframe1">
    <input type="hidden" name="email" id="dataUsersListEmail" />
</form>
<form id="hcpAnnounceListForm" action="http://lists.humanconnectome.org/mailman/subscribe/hcp-announce" target="oframe2">
    <input type="hidden" name="email" id="hcpAnnounceListEmail" />
</form>
<iframe style="display:none" name="oframe1"></iframe>
<iframe style="display:none" name="oframe2"></iframe>



<!-- 
	MODAL: Username / Password Reminder
-->




<div id="forgot_modal" class="x_modal static">
    <div class="box round scroll">
        <div class="title round">
            <span class="inner">ConnectomeDB Username / Password Reminder</span>  <!-- title -->
            <div class="close cancel button"></div>
        </div>
        <div class="body">
            <div class="inner">


                <!-- modal body start -->

                <div id="forgot-pass-div">
                    <div style="">
                        <form id="forgotForm" class="friendlyForm" name="forgotForm" method="post" onsubmit="return forgotValidation()" action="javascript:forgotLoginRequest(this);" accept-charset="UTF-8">
                            <p id="pass-reminder-error" class="error hidden"></p>
							
							<table cellpadding="0" cellspacing="0" border="0" style="margin-top: 30px;">
								<tr valign="top">
									<td style="border-right: 1px solid #e0e0e0; padding-right: 30px;">								
										<p><strong>Forgot your Username?</strong></p>
										<p style="font-size: 12px !important; margin-bottom: 20px;">Your username will be emailed to you.</p>
			
										<div class="inputWrapper">
											<label for="forgotUsername">Enter your email</label> <input type="text" name="email" id="forgotUsername" value="$!email" style="width:225px;" />
										</div>
									</td>
									<td style="padding-left: 30px;">
										<p><strong>Forgot your Password?</strong></p>
										<p style="font-size: 12px !important; margin-bottom: 20px;">A new password will be generated and emailed to you.</p>
										<div class="inputWrapper">
											<label for="forgotPassword">Enter your username</label> <input type="text" name="username" id="forgotPassword" value="$!username" style="width:225px;" />
										</div>
									</td>
								</tr>
							</table>

                            #foreach($key in $data.getParameters().getKeys())
                                #if ($key!="action" && $key!="template" &&$key!="password" &&!$key.startsWith("xdat:user") &&$key!="username" &&$key!="exception")
                                    <input type="hidden" name="$!turbineUtils.escapeParam($key)" value="$!turbineUtils.escapeHTML($!turbineUtils.GetPassedParameter($key,$data))">
                                #end
                            #end
                        </form>
                    </div>
                </div>

                <!-- modal body end -->


            </div>
        </div>
        <div class="footer">
            <div class="inner">
                <span class="buttons">
                    <a class="cancel button" href="javascript:">Cancel</a>  <!-- "Cancel" button text -->
                    <a class="ok default button" href="javascript:">Request</a>  <!-- "OK" button text -->
                </span>
            </div>
        </div>
    </div>
</div>


<!-- 
	MODAL: Registration thank you. 
    If we are using an email confirmation, this is triggered by the successful submission of the registration form. 
--> 

<div id="reg-thanks-div" class="modal hidden">
	<p class="modal-title">Please confirm your registration</p>
    <img class="modal-close" src="$content.getURI('/images/close.gif')" onclick="javascript:modalClose('reg-thanks-div');" />
    <div style="margin:250px 100px; text-align: center;">
        <img src="$content.getURI('/images/logo-icon.png')" />
        <p id="reg-thanks-msg" style="font-size:18px; line-height: 24px;"></p>
	</div>
    <p class="form-submit"><input type="button" value="Close" onclick="javascript:modalClose();"></p>
</div>
<div id="reg-processing-div" class="modal hidden opaque">
    <div style="margin:225px 100px 117px; text-align: center;">
    	<img src="$content.getURI('/images/loading.gif')"/>
	</div>
</div>


<!-- 
	MODAL: Registration confirmation. 
    If we are using an email confirmation, this is triggered by a URL event. 
--> 


<div id="reg-confirm-div" class="modal hidden">
	<p class="modal-title">Registration confirmation</p>
    <img class="modal-close" src="$content.getURI('/images/close.gif')" onclick="javascript:modalClose('reg-confirm-div');" />
    <div style="margin:150px 100px; text-align: center;">
        <img src="$content.getURI('/images/logo-icon.png')" />
        <p id="reg-confirm-msg" style="font-size:18px; line-height: 24px;"></p>
	</div>
    <p class="form-submit"><input type="button" value="Close" onclick="javascript:closeConfirmation();"></p>
</div>
<!-- end page mask and modal -->



<!-- END xnat-templates/screens/Login.vm -->
