var currentProto;

// pick up the protocol and use that to download the latest connect files
// however if we are running off the local file system use http
if (window.location.protocol.indexOf('file') != -1) {
	currentProto = "http:";
} else {
	currentProto = window.location.protocol;
}

var installerPath = currentProto + "//d3gcli72yxqn2z.cloudfront.net/connect/";
var installersLoaded = 0;

$.getScript(installerPath + "asperaweb-2.js", function(data, textStatus, jqxhr) { checkInstallersLoaded(); });
$.getScript(installerPath + "connectversions.js", function(data, textStatus, jqxhr) { checkInstallersLoaded(); });
$.getScript(installerPath + "connectinstaller-2.js", function(data, textStatus, jqxhr) { checkInstallersLoaded(); });

// before initAsperaConnect make sure all three installers have been loaded
var checkInstallersLoaded = function() {
installersLoaded++;
	if (installersLoaded == 3) {
	initAsperaConnect();
	}
}


var handleConnectReady = function() {
       // Called if Aspera Connect is installed and meets version requirements.
  jq('#starting_download').show();
  doDownload();	// callback to function on page to init the download
};

var handleInstallError = function() {
  // Called if an install error occurs. Display some text.
  showMessage('page_body', 'Aspera plugin installation error', 'An error occurred installing the Aspera plugin. Please contact HCP support: support@humanconnectome.org');
};

var handleInstall = function() {
  jq('#no_aspera_plugin').show();
   // Called if an install is required.
   connectInstaller.startEmbeddedInstall({
       installError : handleInstallError,
       stylesheet : currentProto + "//d2fvxkmjao6pcr.cloudfront.net/custom.css",
       installDismiss : function() {
    	//Called if install is dismissed by the user, used in connect 3.0 onwards
        showMessage("page_body", "Notification", "Please install the latest version of the Aspera Connect plugin and restart your web browser.");
    	}    
   });
};


var initAsperaConnect = function () {
	// namespace this guy so he doesn't collide with the one in htmlClose.vm
      try{
	   	 XNAT.app.aspera = {};
         XNAT.app.aspera.connectInstaller = new AW.ConnectInstaller(installerPath);
         XNAT.app.aspera.connectInstaller.init({
             connectReady : handleConnectReady,
             install: handleInstall
         });
      }
      catch(e){
        //catch and just suppress error
      }
};

