/*jslint vars: true, browser: true, white: true */
/*global alert, YAHOO, XNAT, jq */
XNAT.app.filterUI.filterLabelEditor = new function() {"use strict";

	var that = this;

	this.onModification = new YAHOO.util.CustomEvent("modification", this);

	this.init = function() {
		var bd = document.createElement("form");
		bd.setAttribute("onSubmit", "return(false);");
		bd.setAttribute("action", "");
		bd.setAttribute("method", "");
		var table = document.createElement("table");
		table.width="325px";
		var tb = document.createElement("tbody");
		table.appendChild(tb);
		bd.appendChild(table);

		this.labelInput = document.createElement("input");
		this.labelInput.id = "new_label";
		this.labelInput.name = "new_label";
		this.labelInput.maxLength = 256;
		this.labelInput.style.width = "100%";

		var tr = document.createElement("tr");
		var td1 = document.createElement("th");
		var td2 = document.createElement("td");

		td1.innerHTML = "Group name:";
		td1.align = "left";

		td2.appendChild(this.labelInput);
		td2.style.width = "225px";

		tr.appendChild(td1);
		tr.appendChild(td2);
		tb.appendChild(tr);
		
		var tr2 = document.createElement("tr");
		this.td2_2 = document.createElement("td");
		this.td2_2.colSpan = "2";
		this.td2_2.style.color = "red";
		tr2.appendChild(this.td2_2);
		tb.appendChild(tr2);

		this.panel = new YAHOO.widget.Dialog("filterLabelDialog", {
			close : true,
			underlay : "shadow",
			modal : true,
			fixedCenter : true,
			visible : false
		});

		this.panel.handleEnter = function() {
			var label = this.form.new_label;
			var selectedLabel = label.value.trim();
			if (selectedLabel === "") {
				jq(that.td2_2).text("Please specify a name for the group.");
				label.focus();
			} else {
				if(that.duplicateLabelDetected(selectedLabel)) {
					jq(that.td2_2).html("This group name is already in use.<br/>Please choose a different one.");
					label.focus();
					return;
				}

				this.cancel();
				this.selector.onModification.fire(selectedLabel);
			}
		};

		this.panel.handleCancel = function() {
			this.cancel();
		};

		var buttons = [{
			text : "OK",
			handler : {
				fn : this.panel.handleEnter
			},
			isDefault : true
		}, {
			text : "Cancel",
			handler : {
				fn : this.panel.handleCancel
			}
		}];

		var cancelListener = new YAHOO.util.KeyListener(document, {
			keys : 27
		}, {
			fn : this.panel.handleCancel,
			scope : this.panel,
			correctScope : true
		});
		var enterListener = new YAHOO.util.KeyListener(document, {
			keys : 13
		}, {
			fn : this.panel.handleEnter,
			scope : this.panel,
			correctScope : true
		});
		jq(this.labelInput).on("keydown", function() {
			jq(that.td2_2).text("");
		});

		this.panel.setHeader("Save Group");
		this.panel.setBody(bd);
		this.panel.form = bd;
		this.panel.selector = this;
		this.panel.cfg.queueProperty("keyListeners", [cancelListener, enterListener]);
		this.panel.cfg.queueProperty("buttons", buttons);
		this.panel.render("page_body");
	};
	
	this.duplicateLabelDetected = function (newLabel) {
        if (XNAT.app.dashboard.isCurrentFilterAGroup() && !XNAT.app.dashboard.currentGroup.isSiteFilter && newLabel === XNAT.app.dashboard.currentGroup.desc) {
            // if they're just editing the currently group and keeping the name the same, skip the dup check
            // but for a site filter, you must "Save As" (can't use the same name)
            return false;
        }
    	var lC;
        for (lC = 0; lC < XNAT.app.dashboard.previousFiltersLog.length; lC = lC + 1) {
            if (XNAT.app.HcpUtil.isUserMostRecentDownloadGroupName(newLabel)
                || newLabel === XNAT.app.dashboard.previousFiltersLog[lC].desc) {
                return true;
            }
        }
        return false;
	};

	this.show = function(currentFilter) {
		this.labelInput.value = currentFilter ? currentFilter.desc : "";
		jq(this.td2_2).text("");
		this.panel.show();
	};
};
XNAT.app.filterUI.filterLabelEditor.init();
XNAT.app.filterUI.filterLabelEditor.onModification.subscribe(function (eventType, eventArgs) {
        var filterLabel = eventArgs[0];
        XNAT.app.dashboard.logFilter(filterLabel);//this will implicitly reload the groups
        XNAT.app.dashboard.setCurrentGroup(filterLabel);
});
