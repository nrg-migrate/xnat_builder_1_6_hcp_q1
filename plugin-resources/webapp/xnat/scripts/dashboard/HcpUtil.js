/*jslint vars: true, browser: true, white: true */
/*global XNAT */
XNAT.app.HcpUtil = ( function() {"use strict";

		return {
			constants : {
				"USER_MOST_RECENT_DOWNLOAD_GROUP_NAME" : "My Latest Filter"
			},
			isUserMostRecentDownloadGroupName : function(groupName) {
				return this.constants.USER_MOST_RECENT_DOWNLOAD_GROUP_NAME === groupName;
			},
			getUserMostRecentDownloadGroupName : function() {
				return this.constants.USER_MOST_RECENT_DOWNLOAD_GROUP_NAME;
			}
		};

	}());
