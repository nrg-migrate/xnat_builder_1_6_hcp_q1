/*
 * GENERATED FILE
 * Created on Wed Nov 28 13:04:42 CST 2012
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseXnatPetmrsessiondata extends AutoXnatPetmrsessiondata {

	public BaseXnatPetmrsessiondata(ItemI item)
	{
		super(item);
	}

	public BaseXnatPetmrsessiondata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatPetmrsessiondata(UserI user)
	 **/
	public BaseXnatPetmrsessiondata()
	{}

	public BaseXnatPetmrsessiondata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
