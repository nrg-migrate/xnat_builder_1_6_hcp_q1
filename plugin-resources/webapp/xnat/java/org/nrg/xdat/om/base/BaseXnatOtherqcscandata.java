/*
 * GENERATED FILE
 * Created on Wed Sep 26 16:16:24 CDT 2012
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseXnatOtherqcscandata extends AutoXnatOtherqcscandata {

	public BaseXnatOtherqcscandata(ItemI item)
	{
		super(item);
	}

	public BaseXnatOtherqcscandata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatOtherqcscandata(UserI user)
	 **/
	public BaseXnatOtherqcscandata()
	{}

	public BaseXnatOtherqcscandata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
