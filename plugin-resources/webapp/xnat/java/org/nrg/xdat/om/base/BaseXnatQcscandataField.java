/*
 * GENERATED FILE
 * Created on Fri May 25 17:21:00 CDT 2012
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseXnatQcscandataField extends AutoXnatQcscandataField {

	public BaseXnatQcscandataField(ItemI item)
	{
		super(item);
	}

	public BaseXnatQcscandataField(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatQcscandataField(UserI user)
	 **/
	public BaseXnatQcscandataField()
	{}

	public BaseXnatQcscandataField(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
