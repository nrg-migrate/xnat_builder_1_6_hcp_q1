/* 
 *	Copyright Washington University in St Louis 2006
 *	All rights reserved
 * 	
 * 	@author Mohana Ramaratnam (Email: mramarat@wustl.edu)

*/

package org.nrg.pipeline.exception;

@SuppressWarnings("serial")
public class PipelineException extends Exception {
    public PipelineException() {
        super();
    }
}
