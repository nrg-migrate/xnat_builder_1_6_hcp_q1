/**
 * Copyright (c) 2013 Washington University School of Medicine
 */
package org.nrg.xnat.download;

import java.io.File;
import java.nio.file.Path;
import java.util.Iterator;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public abstract class AbstractPackage implements Package {
    private final Path root;
    private Integer count = null;
    private Long size = null;
    
    protected AbstractPackage(final Path root) {
        this.root = root;
    }
    
    protected void computeSizeAndCount() {
        int count = 0;
        long size = 0;
        
        for (final Iterator<Path> pathi = getPathIterator(); pathi.hasNext(); ) {
            final Path path = pathi.next();
            final File f = root.resolve(path).toFile();
            if (f.canRead()) {
                count++;
                size += f.length();
            }
        }
        
        synchronized (this) {
            this.count = count;
            this.size = size;
        }
    }
    
    /* (non-Javadoc)
     * @see org.nrg.xnat.download.Package#getFileCount()
     */
    @Override
    public int getFileCount() {
        if (null == count) {
            computeSizeAndCount();
        }
        return count;
    }

    /* (non-Javadoc)
     * @see org.nrg.xnat.download.Package#getSize()
     */
    @Override
    public long getSize() {
        if (null == size) {
            computeSizeAndCount();
        }
        return size;
    }
}
