/*
 * HCPQ1Package
 * Copyright (c) 2013. Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD License
 */

package org.nrg.xnat.download;

import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Set;

/**
 * PackageDescriptor
 *
 * @author rherri01
 * @since 2/13/13
 */
@XmlRootElement
public class PackageDescriptor {

    @XmlID
    public String getName() {
        return _name;
    }

    public void setName(final String name) {
        _name = name;
    }

    public String getFolder() {
        return _folder;
    }

    public void setFolder(final String folder) {
        _folder = folder;
    }

    public String getLabel() {
        return _label;
    }

    public void setLabel(final String label) {
        _label = label;
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(final String description) {
        _description = description;
    }

    public Set<String> getKeywords() {
        return _keywords;
    }

    public void setKeywords(final Set<String> keywords) {
        _keywords = keywords;
    }

    private String _name;
    private String _folder;
    private String _label;
    private String _description;
    private Set<String> _keywords;

}
