/**
 * Copyright (c) 2013 Washington University School of Medicine
 */
package org.nrg.xnat.download;

import org.nrg.xdat.security.XDATUser;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public interface PackageBuilder {
    Package apply(XDATUser user, String id);
}
