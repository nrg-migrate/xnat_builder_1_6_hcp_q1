/**
 * Copyright (c) 2013 Washington University School of Medicine
 */
package org.nrg.xnat.download;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;

import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.security.XDATUser;
import org.springframework.security.access.AccessDeniedException;

/**
 * Hard-coded package layout and metadata for HCP Q2 release. Yuck.
 *
 * @author Kevin A. Archie <karchie@wustl.edu>
 */
public class HCPQ2PackageBuilderProvider implements PackageBuilderProvider {
    private static final String Q2_PROJECT_NAME = "HCP_Q2";
    private static final String SECURING_ELEMENT = "xnat:mrSessionData/project";

    @Inject
    @Named("packageRoot")
    private File packageRoot;

    @Inject
    @Named("packageSubdir")
    private String packageSubdir;

    private String _suffix;

    // Use a linked hashmap to ensure order of packages.
    private Map<String, PackageDescriptor> _packages = new LinkedHashMap<String, PackageDescriptor>();

    public String getSuffix() {
        return _suffix;
    }

    public void setSuffix(String suffix) {
        _suffix = suffix;
    }

    public void setDescriptors(final List<PackageDescriptor> descriptors) {
        _packages.clear();
        for (PackageDescriptor descriptor : descriptors) {
            _packages.put(descriptor.getName(), descriptor);
        }
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.download.PackageBuilderProvider#getPackageNames()
     */
    public final Set<String> getPackageNames() {
        return _packages.keySet();
    }

    public final String getPackageLabel(final String packageName) {
        return _packages.get(packageName).getLabel();
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.download.PackageBuilderProvider#getPackageDescription(java.lang.String)
     */
    public final String getPackageDescription(final String packageName) {
        return _packages.get(packageName).getDescription();
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.download.PackageBuilderProvider#getPackageKeywords(java.lang.String)
     */
    public final Set<String> getPackageKeywords(final String packageName) {
        return _packages.get(packageName).getKeywords();
    }

    /*
     * (non-Javadoc)
     * @see com.google.common.base.Function#apply(java.lang.Object)
     */
    public PackageBuilder apply(final String packageName) {
        return new HCPQ2ZipFilePackageBuilder(packageRoot.toPath(), packageName, getSuffix());
    }

    public class HCPQ2ZipFilePackageBuilder extends ZipFilePackageBuilder {
        private final String packageName;
        private final String suffix;

        protected class HCPQ2ZipFilePackage extends ZipFilePackage {

            private final String strroot;
            private final String nameFormat;
            private final String folder;

            public HCPQ2ZipFilePackage(final Path root, final String id, final String packageName, final String suffix, Map<String, PackageDescriptor> packages) {
                super(id);
                this.strroot = root.toString();
                this.nameFormat = "%s_" + packageName + "." + suffix;
                this.folder = packages.get(packageName).getFolder();
            }

            @Override
            protected Path makePath(String id) {
                return Paths.get(strroot, packageSubdir, id, folder, String.format(nameFormat, id));
            }
        }

        public HCPQ2ZipFilePackageBuilder(final Path root, final String packageName, final String suffix) {
            super(root);
            this.packageName = packageName;
            this.suffix = suffix;
        }

        @Override
        public Package apply(final XDATUser user, final String id) {
            Throwable cause = null;
            try {
                if (user.canRead(SECURING_ELEMENT, Q2_PROJECT_NAME)) {
                    return new HCPQ2ZipFilePackage(getRoot(), id, packageName, suffix, _packages);
                }
            } catch (Throwable t) {
                cause = t;
            }
            throw new AccessDeniedException("user " + user.getUsername() + " cannot access project " + Q2_PROJECT_NAME, cause);
        }
    }
}
