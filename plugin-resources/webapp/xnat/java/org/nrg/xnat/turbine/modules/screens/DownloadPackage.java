/**
 * Copyright (c) 2013 Washington University
 */
package org.nrg.xnat.turbine.modules.screens;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.ArcArchivespecification;
import org.nrg.xdat.turbine.modules.screens.SecureScreen;
import org.nrg.xnat.turbine.utils.ArcSpecManager;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class DownloadPackage extends SecureScreen {
    /* (non-Javadoc)
     * @see org.apache.turbine.modules.screens.VelocitySecureScreen#doBuildTemplate(org.apache.turbine.util.RunData, org.apache.velocity.context.Context)
     */
    @Override
    protected void doBuildTemplate(final RunData data, final Context context)
            throws MalformedURLException {
        final ArcArchivespecification arc = ArcSpecManager.GetInstance();
        final URL url = new URL(arc.getSiteUrl());
        context.put("webapp", url);   // local host, to start
        
        checkForDemoDownloadParameters(context);
    }
    
    private void checkForDemoDownloadParameters(final Context context) {
	checkForDemoSubjects(context);
	checkForDemoPackages(context);
    }
    
    private void checkForDemoSubjects(final Context context) {
	try {
	    final String demoSubjects = XDAT.getContextService().getBean("demoDownloadSubjects", String.class);
	    context.put("subjects", demoSubjects);
	} catch (final NoSuchBeanDefinitionException e) {
	    // use the real deal
	}
    }

    private void checkForDemoPackages(final Context context) {
	try {
	    final String demoPackages = XDAT.getContextService().getBean("demoDownloadPackages", String.class);
	    context.put("packageIds", demoPackages);
	} catch (final NoSuchBeanDefinitionException e) {
	    // use the real deal
	}
    }
}
