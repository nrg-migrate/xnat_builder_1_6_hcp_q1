/*
 * DownloadPackagesAction
 * Copyright (c) 2013. Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD License
 */

package org.nrg.xnat.turbine.modules.actions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.turbine.modules.actions.SecureAction;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.XFTTableI;
import org.nrg.xnat.restlet.resources.search.MaterializedViewForFilter;

/**
 * DownloadPackagesAction
 *
 * @author rherri01
 * @since 1/22/13
 */
public class DownloadPackagesAction extends SecureAction {
    final private Map<String, String> _parameters = new HashMap<String, String>();

    @Override
    public void doPerform(final RunData data, final Context context) throws Exception {
        initializeParameters(data);

        final List<String> subjects;
        if(StringUtils.isBlank(getParameter("subjects"))) {
            final XFTTableI table = getFilteredCachedResults(data);
            subjects = getSubjectsFromTable(getParameter("keycolumn", "subject_label"), table);
        }
        else {
            subjects = Arrays.asList(getParameter("subjects").split("\\s*,\\s*"));
        }

        if (_log.isDebugEnabled()) {
            _log.debug("Found " + subjects.size() + " subjects to pass to package download component");
        }
        context.put("subjects", subjects);

        preserveVariables(data, context);

        final String destination = getParameter("destination", "XDATScreen_download_packages.vm");

        if (_log.isDebugEnabled()) {
            _log.debug("Setting display screen to: " + destination);
        }
        data.setScreenTemplate(destination);
    }

    private void initializeParameters(final RunData data) {
        final Map<String, String> parameters = TurbineUtils.GetDataParameterHash(data);
        for (final String parameter : parameters.keySet()) {
            _parameters.put(parameter, StringEscapeUtils.unescapeXml(StringEscapeUtils.unescapeHtml(parameters.get(parameter))));
        }
    }

    private String getParameter(final String parameter) {
        return _parameters.get(parameter);
    }

    private String getParameter(final String parameter, final String defaultValue) {
        return _parameters.containsKey(parameter) ? _parameters.get(parameter) : defaultValue;
    }

    private XFTTableI getFilteredCachedResults(final RunData data) throws Exception {
        final XDATUser user = TurbineUtils.getUser(data);
        final String viewId = getParameter("viewid");
        if (_log.isDebugEnabled()) {
            _log.debug("Looking for view ID: " + viewId);
        }
        MaterializedViewForFilter mv = MaterializedViewForFilter.GetMaterializedView(viewId, user);
        if (mv == null) {
            mv = MaterializedViewForFilter.GetMaterializedViewBySearchID(viewId, user);
        }
        if (mv == null) {
            throw new RuntimeException("Unable to locate the materialized view with ID: " + viewId);
        }

        return mv.getData(null, null, null, buildWhere(mv.getColumnNames()));
    }

    /**
     * @param columns A list of columns, usually from a materialized view
     * @return map representing the where clause
     * @throws Exception from MaterializedView.getColumnNames()
     */
    private Map<String, Object> buildWhere(final List<String> columns) throws Exception {
        final Map<String, Object> map = new HashMap<String, Object>();
        for (final String parameter : _parameters.keySet()) {
            if (columns.contains(parameter)) {
                if (_log.isDebugEnabled()) {
                    _log.debug("Found matching column criteria " + parameter + " set to " + _parameters.get(parameter));
                }
                map.put(parameter, _parameters.get(parameter));
            }
        }
        return map;
    }

    private List<String> getSubjectsFromTable(final String keyColumn, final XFTTableI table) {
        final List<String> subjects = new ArrayList<String>();
        final String[] columns = table.getColumns();
        int index = 0;
        for (; index < columns.length; index++) {
            if (columns[index].equalsIgnoreCase(keyColumn)) {
                break;
            }
        }
        if (index == columns.length) {
            throw new RuntimeException("Couldn't find the requested key column in the table results: " + keyColumn);
        }
        if (_log.isDebugEnabled()) {
            _log.debug("Found " + subjects.size() + " for the key column: " + keyColumn);
        }

        while (table.hasMoreRows()) {
            final String subject = table.nextRow()[index].toString();
            if (_log.isDebugEnabled()) {
                _log.debug("Adding subject ID: " + subject);
            }
            subjects.add(subject);
        }
        return subjects;
    }

    private static final Log _log = LogFactory.getLog(DownloadPackagesAction.class);
}
