//Copyright 2006 Harvard University / Washington University School of Medicine All Rights Reserved
/*
 * Created on Oct 17, 2006
 *
 */
package org.nrg.xnat.turbine.modules.actions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.turbine.modules.actions.SecureAction;
import org.nrg.xdat.turbine.utils.TurbineUtils;

public class ChooseDownloadPackages extends SecureAction {

    @Override
    public void doPerform(final RunData data, final Context context) {
	context.put("subjects", StringUtils.join(getSubjects(data), ","));
	context.put("packageIds", getPackageIds(data));
	context.put("StringUtils", new StringUtils());
	data.setScreenTemplate("DownloadPackage.vm");
    }

    private List<String> getSubjects(final RunData data) {
	final String[] passedSubjects = (String[]) TurbineUtils.GetPassedObjects("subject", data);
	if (null == passedSubjects) {
	    return new ArrayList<String>();
	} else {
	    return Arrays.asList(passedSubjects);
	}
    }

    private List<String> getPackageIds(final RunData data) {
	String packageIdCsv = (String) TurbineUtils.GetPassedParameter("packageIdCsv", data);
	return Arrays.asList(packageIdCsv.split("\\s*,\\s*"));
    }
}
