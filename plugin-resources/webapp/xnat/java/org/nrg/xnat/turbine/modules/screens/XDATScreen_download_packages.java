package org.nrg.xnat.turbine.modules.screens;

import org.apache.commons.lang.StringUtils;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.turbine.modules.screens.SecureScreen;

public class XDATScreen_download_packages extends SecureScreen {

    @Override
    protected void doBuildTemplate(final RunData data, final Context context) throws Exception {
        context.put("projectId", getProjectId(data));
        final String query = data.getParameters().get("query");
        if (!StringUtils.isBlank(query)) {
            context.put("query", query);
        }
        // the action that calls us will have put the subjectIds in the context for us
    }

    private String getProjectId(final RunData data) {
        return "ALL"; // for this go-round we will not be in the context of a particular project
    }
}
