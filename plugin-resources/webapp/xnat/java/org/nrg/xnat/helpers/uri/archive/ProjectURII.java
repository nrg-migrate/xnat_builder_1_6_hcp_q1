package org.nrg.xnat.helpers.uri.archive;

import org.nrg.xdat.om.XnatProjectdata;

public interface ProjectURII {
	public XnatProjectdata getProject();
}
