package org.nrg.xnat.helpers.uri.archive;

import org.nrg.xdat.om.XnatImagesessiondata;

public interface AssessedURII {
	public XnatImagesessiondata getSession();
}
