package org.nrg.xnat.helpers.uri.archive;

import org.nrg.xdat.om.XnatSubjectdata;

public interface SubjectURII {
	public XnatSubjectdata getSubject();
}
