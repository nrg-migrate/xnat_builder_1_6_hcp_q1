package org.nrg.xnat.helpers.uri.archive;

import org.nrg.xdat.om.XnatReconstructedimagedata;

public interface ReconURII {
	public XnatReconstructedimagedata getRecon();
}
