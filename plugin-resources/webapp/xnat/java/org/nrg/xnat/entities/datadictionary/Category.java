/*
 * Category
 * Copyright (c) 2013. Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD License
 */

package org.nrg.xnat.entities.datadictionary;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Represents a category.
 *
 * @author rherri01
 * @since 2/18/13
 */
public class Category extends Node {
    public Category() {
        super();
    }

    public Category(final int position, final String name, final String columnHeader, final String description, final boolean restricted, final List<String> projects) {
        super(position, name, columnHeader, description, restricted, projects);
    }

    private static final long serialVersionUID = 4351354641917131046L;
}
