/*
 * Node
 * Copyright (c) 2013. Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD License
 */

package org.nrg.xnat.entities.datadictionary;

import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * This class is the most atomic representation of an entity in the XNAT data dictionary.
 *
 * @author rherri01
 * @since 2/18/13
 */
@XmlRootElement
public class Node implements Serializable {

    public Node() {
        // Default constructor
    }

    public Node(final int position, final String name, final String columnHeader, final String description, final boolean restricted, final List<String> projects) {
        setPosition(position);
        setName(name);
        setColumnHeader(columnHeader);
        setDescription(description);
        setRestricted(restricted);
        setProjects(projects);
    }

    @XmlID
    public String getSystemId() {
        return getClass().getName() + "/" + _name;
    }

    /**
     * TODO: Position is a <i>total hack</i>. Replace this with linked nodes: siblings, parents, and children.
     * @return This node's relative position in relation to its siblings.
     */
    public int getPosition() {
        return _position;
    }

    public void setPosition(int position) {
        _position = position;
    }

    public String getName() {
        return _name;
    }

    public void setName(final String name) {
        _name = name;
    }

    public String getColumnHeader() {
        return _columnHeader;
    }

    public void setColumnHeader(final String columnHeader) {
        _columnHeader = columnHeader;
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(String description) {
        _description = description;
    }

    public boolean getRestricted() {
        return _restricted;
    }

    public void setRestricted(boolean _restricted) {
        this._restricted = _restricted;
    }

    public List<String> getProjects() {
        return _projects;
    }

    public void setProjects(final List<String> _projects) {
        this._projects = _projects;
    }

    /**
     * Used for sorting of nodes based on the position property.
     *
     * @author rherri01
     * @since 2/19/13
     */
    public static class NodeComparator implements Comparator<Node> {
        @Override
        public int compare(final Node node1, final Node node2) {
            return node1.getPosition() == node2.getPosition() ? 0 : (node1.getPosition() < node2.getPosition() ? -1 : 1);
        }
    }

    private static final long serialVersionUID = 3074427465020840486L;

    private int _position;
    private String _name;
    private String _columnHeader;
    private String _description;
    private boolean _restricted;
    private List<String> _projects;
}
