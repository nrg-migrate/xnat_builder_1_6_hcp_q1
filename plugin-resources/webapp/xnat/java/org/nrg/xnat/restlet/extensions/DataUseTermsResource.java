package org.nrg.xnat.restlet.extensions;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.resource.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.StringRepresentation;
import org.restlet.resource.Variant;

/**
 * DatauseRestlet
 * 
 * This is a dummy restlet that will be overlayed by the real, HCP extension during the build process.
 */
@XnatRestlet({
    "/services/datause"
})
public class DataUseTermsResource extends SecureResource {

    // key format is "userid-dataset", e.g. "ehaas01-Phase2"
    private static Map<String, Boolean> acceptedTerms;

    static {
	acceptedTerms = new HashMap<String, Boolean>();
    }

    public DataUseTermsResource(Context context, Request request, Response response) {
        super(context, request, response);
        this.getVariants().add(new Variant(MediaType.ALL));
    }

    @Override
    public Representation represent(Variant variant) throws ResourceException {
	final Representation r = new StringRepresentation(String.valueOf(acceptedTerms.containsKey(getKey())));
	r.setMediaType(MediaType.TEXT_PLAIN);
	return r;
    }
    
    private String getKey() {
	final String dataset = getQueryVariable("terms");
	return user.getUsername().concat("-").concat(dataset);
    }

    @Override
    public boolean allowPost() {
	return true;
    }

    @Override
    public void handlePost() {
	if (Boolean.parseBoolean(StringUtils.defaultString(getQueryVariable("acceptTerms")))) {
	    acceptedTerms.put(getKey(), true);
	}
	returnDefaultRepresentation();
    }
}
